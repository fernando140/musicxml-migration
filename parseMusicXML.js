/* Had to fake a get-fetch request to localhost to get the musicxml file */

const xmlFileUrl = "http://127.0.0.1:8000/Arkansas-Traveller.musicxml";
// const xmlFileUrl = "http://127.0.0.1:8000/Black-Mountain-Rag.musicxml";
// const xmlFileUrl = "http://127.0.0.1:8000/Billy-in-the-Lowground.musicxml";
// const xmlFileUrl = "http://127.0.0.1:8000/Wayfaring_Stranger-upload-uncompressed-musicxml-1bar-count-in-120BPM.musicxml";
// const xmlFileUrl = "http://127.0.0.1:8000/Blue-Moon-of-Kentucky.musicxml";
// const xmlFileUrl = "http://127.0.0.1:8000/Wildwood-Flower-Crosspicking-without-tab-stems.musicxml";


import { parseString } from "xml2js";

let song = [];
let m = 0;
let timeCounter = 0;
const bpm = 120;

// Dot value extends the note duration by half
// e.g. if it's quarter is duration will be
// quarter + eighth (5/16)
const setDotValue = (note) => {
    switch (note.type) {
        case "half":
            return "quarter";
        case "quarter":
            return "eighth";
        case "eighth":
            return "16th";
    }
};

const setChordTime = (song) => {
    for(let i = 0; i < song.length; i++) {
        if (song[i].isChord && i > 0) {
            if(song[i].defaultXPos === song[i-1].defaultXPos) {
                song[i].time = song[i - 1].time;
            }
        }
    }
    return song;
}

const mutateSongForPlayer = (finalSong) => {
    let mutatedSong = [];
    for (let i = 0; i < finalSong.length; i++) {
        if (finalSong[i]?.chordIdx === "last") {
            mutatedSong[i] = [finalSong[i]];
            for(let j = 1; j < 4; j++) { // Check last 3 notes in case they are same chord
                if (finalSong[i-j]?.chordIdx === "not-last" && (finalSong[i-j]?.defaultXPos === finalSong[i].defaultXPos)) {
                    mutatedSong[i].unshift(finalSong[i - j]);
                }
            }
        } else {
            mutatedSong[i] = [finalSong[i]];
        }
    }
    return mutatedSong;
}

const incrementTimer = (noteDuration, dot = false, bpm) => {
    const msPerBeat = 60 / bpm;

    // for a 4/4 song
    const noteLength = {
        complete: msPerBeat * 4,
        whole: msPerBeat * 4,
        half: msPerBeat * 2,
        quarter: msPerBeat * 1,
        eighth: msPerBeat * 0.5,
        "16th": msPerBeat * 0.25,
        "32th": msPerBeat * 0.125,
    };

    switch (noteDuration) {
        case "complete":
            return (timeCounter += !dot
                ? noteLength.complete
                : noteLength.complete * 1.5);
        case "whole":
            return (timeCounter += !dot
                ? noteLength.whole
                : noteLength.whole * 1.5);
        case "half":
            return (timeCounter += !dot
                ? noteLength.half
                : noteLength.half * 1.5);
        case "quarter":
            return (timeCounter += !dot
                ? noteLength.quarter
                : noteLength.quarter * 1.5);
        case "eighth":
            return (timeCounter += !dot
                ? noteLength.eighth
                : noteLength.eighth * 1.5);
        case "16th":
            return (timeCounter += !dot
                ? noteLength["16th"]
                : noteLength["16th"] * 1.5);
        case "32th":
            return (timeCounter += !dot
                ? noteLength["32th"]
                : noteLength.complete * 1.5);
        default:
            break;
    }
};

const getLastNoteLength = () => {
    const onlyNotesAndRests = getOnlyNotesAndRests();
    const lastNoteLength =
        onlyNotesAndRests[onlyNotesAndRests.length - 1].duration;
    return lastNoteLength;
};

const getOnlyNotesAndRests = () => song.filter((el) => el.isRest || el.name);

const timerAction = (m, hasDot, bpm) => {
    if (m === 1) return 0;

    const partialSong = getOnlyNotesAndRests();
    if (
        m > 1 &&
        partialSong[partialSong.length - 2]?.addToTimer !== undefined
    ) {
        return timeCounter;
    }

    const incrementNormally = incrementTimer(getLastNoteLength(), hasDot, bpm);
    return incrementNormally;
};

const pushSymbol = (count) => {
    for(let i = 0; i < count; i++) {
        song.push({
            isLine: false,
            color: "white",
            show: false,
            duration: 0,
            time: 0,
            offset: 0,
            isSymbol: true,
        });
    }
}

export const fetchXML = async () => {
    const parseXML = async () => {
        const result = await fetch(xmlFileUrl);
        if (!result.ok) {
            console.error("Error fetching file");
            return;
        }

        const xml = await result.text();
        parseString(xml, { explicitArray: false }, (error, result) => {
            console.dir(result);

            const parsedSong = result;

            pushSymbol(2);

            parsedSong["score-partwise"]?.part[1]?.measure.map((ms, idx) => {
                // starting measure line
                song.push({
                    isLine: true,
                    location: "start",
                    show: false,
                    time: 0,
                    duration: 0,
                    measure: idx + 1,
                });

                // set repeater if any
                if (ms?.barline?.repeat !== undefined) {
                    song.push({
                        repeater: true,
                        direction: ms.barline.repeat.$.direction,
                        color: 'white',
                        show: false,
                        measure: idx + 1,
                        color: 'white',
                        show: false,
                    });
                }
                // loop through notes from the measure
                ms.note.map(async(nt) => {

                    const songWithOnlyNotes = getOnlyNotesAndRests();
                    let lastNote = songWithOnlyNotes[songWithOnlyNotes.length - 1];
                    const lastNoteHasDot = lastNote?.dot ? true : false;
                    const actualNoteHasDot =
                        nt?.dot === "" ? setDotValue(nt) : false;

                    // check if note is rest -silence- note
                    if (nt?.rest === "" || nt?.rest) {
                        // only if rest is staff == 1 add
                        // otherwise there's repetition of the note
                        if (nt.staff == 1) {
                            m += 1;
                            const restLength = nt?.type ?? "complete";
                            song.push({
                                color: 'white',
                                show: false,
                                isRest: true,
                                dot: actualNoteHasDot,
                                duration: restLength,
                                measure: idx + 1,
                                staff: nt?.staff ?? false,
                                time: timerAction(m, lastNoteHasDot, bpm),
                            });
                        }
                    }

                    // check if it's tab note
                    if (nt?.notations?.technical?.fret !== undefined) {
                        m += 1;
                        const noteLength = nt?.type;
                        let isChord = false
                        if (m > 1) { // avoid breaking if it's first note
                            isChord = nt?.$["default-x"] === lastNote.defaultXPos
                                        ? true
                                        : false;
                        }
                        song.push({
                            defaultXPos: nt?.$["default-x"],
                            isChord: isChord,
                            chordIdx: isChord ? 'last' : false,
                            tie: nt?.tie ?? false,
                            pitch: `${nt?.pitch?.step}${nt?.pitch?.octave}`,
                            dot: actualNoteHasDot,
                            name: {
                                position: parseInt(nt?.notations?.technical?.fret),
                                string: parseInt(nt?.notations?.technical?.string),
                            },
                            color: 'white',
                            show: false,
                            duration: noteLength,
                            measure: idx + 1,
                            time: timerAction(m, lastNoteHasDot, bpm),
                        });
                        if (nt?.$["default-x"] === lastNote?.defaultXPos && m > 1) {
                            lastNote.isChord = true;
                            lastNote.addToTimer = false;
                            lastNote.chordIdx = 'not-last';
                        }
                    }
                });

                // set the end of the repeater, if any
                if (
                    ms?.barline !== undefined &&
                    ms?.barline.length > 1 &&
                    ms?.barline[1].repeat !== undefined
                ) {
                    song.push({
                        repeater: true,
                        direction: ms.barline[1].repeat.$.direction,
                        measure: idx + 1,
                        color: 'white',
                        show: false,
                    });
                }
            });

            const finalSong = setChordTime(song);
            const mutatedSong = mutateSongForPlayer(finalSong);

            return mutatedSong;
        });
    };
    parseXML();
};
