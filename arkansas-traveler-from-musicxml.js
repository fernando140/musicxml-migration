// This is the object from the parsing
// of the musicXML file. It does match 
// with the structure for the videoPlayer

// song: Arkansas Traveler

const objectFromMusicXml = [
    [
        {
            "isLine": false,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 0,
            "offset": 0,
            "isSymbol": true
        }
    ],
    [
        {
            "isLine": false,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 0,
            "offset": 0,
            "isSymbol": true
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 1
        }
    ],
    [
        {
            "color": "white",
            "show": false,
            "isRest": true,
            "dot": false,
            "duration": "eighth",
            "measure": 1,
            "staff": "1",
            "time": 0
        }
    ],
    [
        {
            "defaultXPos": "167.95",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A3",
            "dot": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "16th",
            "measure": 1,
            "time": 0.25
        }
    ],
    [
        {
            "defaultXPos": "191.58",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "B3",
            "dot": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "16th",
            "measure": 1,
            "time": 0.375
        }
    ],
    [
        {
            "defaultXPos": "215.22",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "C4",
            "dot": false,
            "name": {
                "position": 6,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "16th",
            "measure": 1,
            "time": 0.5
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 2
        }
    ],
    [
        {
            "repeater": true,
            "direction": "forward",
            "color": "white",
            "show": false,
            "measure": 2
        }
    ],
    [
        {
            "defaultXPos": "32.07",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 2,
            "time": 0.625
        }
    ],
    [
        {
            "defaultXPos": "68.72",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F4",
            "dot": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 2,
            "time": 0.875
        }
    ],
    [
        {
            "defaultXPos": "105.38",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 2,
            "time": 1.125
        }
    ],
    [
        {
            "defaultXPos": "142.03",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 2,
            "time": 1.375
        }
    ],
    [
        {
            "defaultXPos": "178.69",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "B3",
            "dot": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 2,
            "time": 1.625
        }
    ],
    [
        {
            "defaultXPos": "237.34",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "B3",
            "dot": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 2,
            "time": 2.125,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "237.34",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "B3",
            "dot": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 2,
            "time": 2.125,
            "addToTimer": false
        },
        {
            "defaultXPos": "237.34",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 2,
            "time": 2.125
        }
    ],
    [
        {
            "defaultXPos": "273.99",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "B3",
            "dot": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 2,
            "time": 2.375,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "273.99",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "B3",
            "dot": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 2,
            "time": 2.375,
            "addToTimer": false
        },
        {
            "defaultXPos": "273.99",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 2,
            "time": 2.375
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 3
        }
    ],
    [
        {
            "defaultXPos": "18.30",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A3",
            "dot": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 3,
            "time": 2.625
        }
    ],
    [
        {
            "defaultXPos": "75.54",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "A3",
            "dot": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 3,
            "time": 3.125,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "75.54",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "A3",
            "dot": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 3,
            "time": 3.125,
            "addToTimer": false
        },
        {
            "defaultXPos": "75.54",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 3,
            "time": 3.125
        }
    ],
    [
        {
            "defaultXPos": "111.32",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "A3",
            "dot": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 3,
            "time": 3.375,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "111.32",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "A3",
            "dot": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 3,
            "time": 3.375,
            "addToTimer": false
        },
        {
            "defaultXPos": "111.32",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 3,
            "time": 3.375
        }
    ],
    [
        {
            "defaultXPos": "147.09",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 3,
            "time": 3.625
        }
    ],
    [
        {
            "defaultXPos": "204.33",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 3,
            "time": 4.125,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "204.33",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 3,
            "time": 4.125,
            "addToTimer": false
        },
        {
            "defaultXPos": "204.33",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 3,
            "time": 4.125
        }
    ],
    [
        {
            "defaultXPos": "240.11",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 3,
            "time": 4.375,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "240.11",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 3,
            "time": 4.375,
            "addToTimer": false
        },
        {
            "defaultXPos": "240.11",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 3,
            "time": 4.375
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 4
        }
    ],
    [
        {
            "defaultXPos": "15.42",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 4,
            "time": 4.625
        }
    ],
    [
        {
            "defaultXPos": "69.06",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 4,
            "time": 5.125,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "69.06",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 4,
            "time": 5.125,
            "addToTimer": false
        },
        {
            "defaultXPos": "69.06",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 4,
            "time": 5.125
        }
    ],
    [
        {
            "defaultXPos": "102.58",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 4,
            "time": 5.375,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "102.58",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 4,
            "time": 5.375,
            "addToTimer": false
        },
        {
            "defaultXPos": "102.58",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 4,
            "time": 5.375
        }
    ],
    [
        {
            "defaultXPos": "136.11",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F4",
            "dot": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 4,
            "time": 5.625
        }
    ],
    [
        {
            "defaultXPos": "189.75",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "F4",
            "dot": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 4,
            "time": 6.125,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "189.75",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "F4",
            "dot": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 4,
            "time": 6.125,
            "addToTimer": false
        },
        {
            "defaultXPos": "189.75",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 4,
            "time": 6.125
        }
    ],
    [
        {
            "defaultXPos": "223.28",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "F4",
            "dot": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 4,
            "time": 6.375,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "223.28",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "F4",
            "dot": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 4,
            "time": 6.375,
            "addToTimer": false
        },
        {
            "defaultXPos": "223.28",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 4,
            "time": 6.375
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 5
        }
    ],
    [
        {
            "defaultXPos": "100.66",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 5,
            "time": 6.625
        }
    ],
    [
        {
            "defaultXPos": "130.65",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F4",
            "dot": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 5,
            "time": 6.875
        }
    ],
    [
        {
            "defaultXPos": "160.64",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 5,
            "time": 7.125
        }
    ],
    [
        {
            "defaultXPos": "190.63",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 5,
            "time": 7.375
        }
    ],
    [
        {
            "defaultXPos": "220.62",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "B3",
            "dot": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 5,
            "time": 7.625
        }
    ],
    [
        {
            "defaultXPos": "268.60",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A3",
            "dot": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 5,
            "time": 8.125
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 6
        }
    ],
    [
        {
            "defaultXPos": "15.42",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 6,
            "time": 8.625
        }
    ],
    [
        {
            "defaultXPos": "47.50",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F4",
            "dot": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 6,
            "time": 8.875
        }
    ],
    [
        {
            "defaultXPos": "79.59",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 6,
            "time": 9.125
        }
    ],
    [
        {
            "defaultXPos": "111.67",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 6,
            "time": 9.375
        }
    ],
    [
        {
            "defaultXPos": "143.75",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "B3",
            "dot": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 6,
            "time": 9.625
        }
    ],
    [
        {
            "defaultXPos": "195.09",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "B3",
            "dot": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 6,
            "time": 10.125,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "195.09",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "B3",
            "dot": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 6,
            "time": 10.125,
            "addToTimer": false
        },
        {
            "defaultXPos": "195.09",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 6,
            "time": 10.125
        }
    ],
    [
        {
            "defaultXPos": "227.17",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "B3",
            "dot": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 6,
            "time": 10.375,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "227.17",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "B3",
            "dot": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 6,
            "time": 10.375,
            "addToTimer": false
        },
        {
            "defaultXPos": "227.17",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 6,
            "time": 10.375
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 7
        }
    ],
    [
        {
            "defaultXPos": "18.30",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A3",
            "dot": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 7,
            "time": 10.625
        }
    ],
    [
        {
            "defaultXPos": "67.82",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "A3",
            "dot": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 7,
            "time": 11.125,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "67.82",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "A3",
            "dot": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 7,
            "time": 11.125,
            "addToTimer": false
        },
        {
            "defaultXPos": "67.82",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 7,
            "time": 11.125
        }
    ],
    [
        {
            "defaultXPos": "98.77",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "A3",
            "dot": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 7,
            "time": 11.375,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "98.77",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "A3",
            "dot": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 7,
            "time": 11.375,
            "addToTimer": false
        },
        {
            "defaultXPos": "98.77",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 7,
            "time": 11.375
        }
    ],
    [
        {
            "defaultXPos": "129.72",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 7,
            "time": 11.625
        }
    ],
    [
        {
            "defaultXPos": "179.24",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 7,
            "time": 12.125,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "179.24",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 7,
            "time": 12.125,
            "addToTimer": false
        },
        {
            "defaultXPos": "179.24",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 7,
            "time": 12.125
        }
    ],
    [
        {
            "defaultXPos": "210.19",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 7,
            "time": 12.375,
            "addToTimer": false
        }
    ],
    [
        {
            "defaultXPos": "210.19",
            "isChord": true,
            "chordIdx": "not-last",
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 7,
            "time": 12.375,
            "addToTimer": false
        },
        {
            "defaultXPos": "210.19",
            "isChord": true,
            "chordIdx": "last",
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 7,
            "time": 12.375
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 8
        }
    ],
    [
        {
            "defaultXPos": "15.42",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 8,
            "time": 12.625
        }
    ],
    [
        {
            "defaultXPos": "46.50",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "C5",
            "dot": false,
            "name": {
                "position": 4,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 8,
            "time": 12.875
        }
    ],
    [
        {
            "defaultXPos": "77.58",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 8,
            "time": 13.125
        }
    ],
    [
        {
            "defaultXPos": "108.66",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 8,
            "time": 13.375
        }
    ],
    [
        {
            "defaultXPos": "139.74",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "B4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 8,
            "time": 13.625
        }
    ],
    [
        {
            "defaultXPos": "170.82",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "C5",
            "dot": false,
            "name": {
                "position": 4,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 8,
            "time": 13.875
        }
    ],
    [
        {
            "defaultXPos": "201.90",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 8,
            "time": 14.125
        }
    ],
    [
        {
            "defaultXPos": "232.98",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "B4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 8,
            "time": 14.375
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 9
        }
    ],
    [
        {
            "defaultXPos": "100.66",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 9,
            "time": 14.625
        }
    ],
    [
        {
            "defaultXPos": "131.23",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F4",
            "dot": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 9,
            "time": 14.875
        }
    ],
    [
        {
            "defaultXPos": "161.79",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "G4",
            "dot": false,
            "name": {
                "position": 5,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 9,
            "time": 15.125
        }
    ],
    [
        {
            "defaultXPos": "192.35",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 9,
            "time": 15.375
        }
    ],
    [
        {
            "defaultXPos": "222.91",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 9,
            "time": 15.625
        }
    ],
    [
        {
            "color": "white",
            "show": false,
            "isRest": true,
            "dot": false,
            "duration": "eighth",
            "measure": 9,
            "staff": "1",
            "time": 16.125
        }
    ],
    [
        {
            "defaultXPos": "293.44",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A3",
            "dot": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "16th",
            "measure": 9,
            "time": 16.375
        }
    ],
    [
        {
            "defaultXPos": "316.70",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "B3",
            "dot": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "16th",
            "measure": 9,
            "time": 16.5
        }
    ],
    [
        {
            "defaultXPos": "339.97",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "C4",
            "dot": false,
            "name": {
                "position": 6,
                "string": 4
            },
            "color": "white",
            "show": false,
            "duration": "16th",
            "measure": 9,
            "time": 16.625
        }
    ],
    [
        {
            "repeater": true,
            "direction": "backward",
            "measure": 9,
            "color": "white",
            "show": false
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 10
        }
    ],
    [
        {
            "defaultXPos": "15.42",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 10,
            "time": 16.75
        }
    ],
    [
        {
            "defaultXPos": "44.73",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F4",
            "dot": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 10,
            "time": 17
        }
    ],
    [
        {
            "defaultXPos": "74.05",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "G4",
            "dot": false,
            "name": {
                "position": 5,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 10,
            "time": 17.25
        }
    ],
    [
        {
            "defaultXPos": "103.37",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 10,
            "time": 17.5
        }
    ],
    [
        {
            "defaultXPos": "132.69",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 10,
            "time": 17.75
        }
    ],
    [
        {
            "defaultXPos": "179.59",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F5",
            "dot": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 10,
            "time": 18.25
        }
    ],
    [
        {
            "defaultXPos": "208.91",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "G5",
            "dot": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 10,
            "time": 18.5
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 11
        }
    ],
    [
        {
            "repeater": true,
            "direction": "forward",
            "color": "white",
            "show": false,
            "measure": 11
        }
    ],
    [
        {
            "defaultXPos": "32.07",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 11,
            "time": 18.75
        }
    ],
    [
        {
            "defaultXPos": "61.70",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "G5",
            "dot": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 11,
            "time": 19
        }
    ],
    [
        {
            "defaultXPos": "91.34",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F5",
            "dot": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 11,
            "time": 19.25
        }
    ],
    [
        {
            "defaultXPos": "120.97",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 11,
            "time": 19.5
        }
    ],
    [
        {
            "defaultXPos": "150.61",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "G5",
            "dot": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 11,
            "time": 19.75
        }
    ],
    [
        {
            "defaultXPos": "180.25",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F5",
            "dot": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 11,
            "time": 20
        }
    ],
    [
        {
            "defaultXPos": "209.88",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E5",
            "dot": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 11,
            "time": 20.25
        }
    ],
    [
        {
            "defaultXPos": "239.52",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "G5",
            "dot": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 11,
            "time": 20.5
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 12
        }
    ],
    [
        {
            "defaultXPos": "15.42",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F5",
            "dot": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 12,
            "time": 20.75
        }
    ],
    [
        {
            "defaultXPos": "40.39",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E5",
            "dot": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 12,
            "time": 21
        }
    ],
    [
        {
            "defaultXPos": "65.37",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 12,
            "time": 21.25
        }
    ],
    [
        {
            "defaultXPos": "90.34",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F5",
            "dot": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 12,
            "time": 21.5
        }
    ],
    [
        {
            "defaultXPos": "115.31",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E5",
            "dot": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 12,
            "time": 21.75
        }
    ],
    [
        {
            "defaultXPos": "155.27",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 12,
            "time": 22.25
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 13
        }
    ],
    [
        {
            "defaultXPos": "100.66",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 13,
            "time": 22.75
        }
    ],
    [
        {
            "defaultXPos": "147.44",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 13,
            "time": 23.25
        }
    ],
    [
        {
            "defaultXPos": "176.67",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 13,
            "time": 23.5
        }
    ],
    [
        {
            "defaultXPos": "205.91",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E5",
            "dot": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 13,
            "time": 23.75
        }
    ],
    [
        {
            "defaultXPos": "252.68",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E5",
            "dot": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 13,
            "time": 24.25
        }
    ],
    [
        {
            "defaultXPos": "281.92",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E5",
            "dot": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 13,
            "time": 24.5
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 14
        }
    ],
    [
        {
            "defaultXPos": "15.42",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F5",
            "dot": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 14,
            "time": 24.75
        }
    ],
    [
        {
            "defaultXPos": "47.32",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E5",
            "dot": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 14,
            "time": 25
        }
    ],
    [
        {
            "defaultXPos": "79.23",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 14,
            "time": 25.25
        }
    ],
    [
        {
            "defaultXPos": "111.14",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F5",
            "dot": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 14,
            "time": 25.5
        }
    ],
    [
        {
            "defaultXPos": "143.04",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E5",
            "dot": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 14,
            "time": 25.75
        }
    ],
    [
        {
            "defaultXPos": "194.09",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F5",
            "dot": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 14,
            "time": 26.25
        }
    ],
    [
        {
            "defaultXPos": "226.00",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "G5",
            "dot": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 14,
            "time": 26.5
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 15
        }
    ],
    [
        {
            "defaultXPos": "18.30",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 15,
            "time": 26.75
        }
    ],
    [
        {
            "defaultXPos": "51.77",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "G5",
            "dot": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 15,
            "time": 27
        }
    ],
    [
        {
            "defaultXPos": "85.24",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F5",
            "dot": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 15,
            "time": 27.25
        }
    ],
    [
        {
            "defaultXPos": "118.71",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 15,
            "time": 27.5
        }
    ],
    [
        {
            "defaultXPos": "152.18",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "G5",
            "dot": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 15,
            "time": 27.75
        }
    ],
    [
        {
            "defaultXPos": "185.66",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F5",
            "dot": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 15,
            "time": 28
        }
    ],
    [
        {
            "defaultXPos": "219.13",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E5",
            "dot": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 15,
            "time": 28.25
        }
    ],
    [
        {
            "defaultXPos": "252.60",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "G5",
            "dot": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 15,
            "time": 28.5
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 16
        }
    ],
    [
        {
            "defaultXPos": "15.42",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F5",
            "dot": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 16,
            "time": 28.75
        }
    ],
    [
        {
            "defaultXPos": "44.65",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E5",
            "dot": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 16,
            "time": 29
        }
    ],
    [
        {
            "defaultXPos": "73.89",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 16,
            "time": 29.25
        }
    ],
    [
        {
            "defaultXPos": "103.12",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F5",
            "dot": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 16,
            "time": 29.5
        }
    ],
    [
        {
            "defaultXPos": "132.36",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E5",
            "dot": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 16,
            "time": 29.75
        }
    ],
    [
        {
            "defaultXPos": "179.13",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 16,
            "time": 30.25
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 17
        }
    ],
    [
        {
            "defaultXPos": "100.66",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 17,
            "time": 30.75
        }
    ],
    [
        {
            "defaultXPos": "141.31",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "C5",
            "dot": false,
            "name": {
                "position": 4,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 17,
            "time": 31
        }
    ],
    [
        {
            "defaultXPos": "181.95",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 17,
            "time": 31.25
        }
    ],
    [
        {
            "defaultXPos": "222.60",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 17,
            "time": 31.5
        }
    ],
    [
        {
            "defaultXPos": "263.24",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "B4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 17,
            "time": 31.75
        }
    ],
    [
        {
            "defaultXPos": "303.89",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "C5",
            "dot": false,
            "name": {
                "position": 4,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 17,
            "time": 32
        }
    ],
    [
        {
            "defaultXPos": "344.53",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D5",
            "dot": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 17,
            "time": 32.25
        }
    ],
    [
        {
            "defaultXPos": "385.18",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "B4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 17,
            "time": 32.5
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 18
        }
    ],
    [
        {
            "defaultXPos": "15.42",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 18,
            "time": 32.75
        }
    ],
    [
        {
            "defaultXPos": "58.33",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F4",
            "dot": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 18,
            "time": 33
        }
    ],
    [
        {
            "defaultXPos": "101.25",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "G4",
            "dot": false,
            "name": {
                "position": 5,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 18,
            "time": 33.25
        }
    ],
    [
        {
            "defaultXPos": "144.16",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 18,
            "time": 33.5
        }
    ],
    [
        {
            "defaultXPos": "187.08",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "quarter",
            "measure": 18,
            "time": 33.75
        }
    ],
    [
        {
            "defaultXPos": "255.74",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F5",
            "dot": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 18,
            "time": 34.25
        }
    ],
    [
        {
            "defaultXPos": "298.66",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "G5",
            "dot": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 18,
            "time": 34.5
        }
    ],
    [
        {
            "repeater": true,
            "direction": "backward",
            "measure": 18,
            "color": "white",
            "show": false
        }
    ],
    [
        {
            "isLine": true,
            "location": "start",
            "show": false,
            "time": 0,
            "duration": 0,
            "measure": 19
        }
    ],
    [
        {
            "defaultXPos": "15.42",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "A4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 19,
            "time": 34.75
        }
    ],
    [
        {
            "defaultXPos": "59.47",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "F4",
            "dot": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 19,
            "time": 35
        }
    ],
    [
        {
            "defaultXPos": "103.53",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "G4",
            "dot": false,
            "name": {
                "position": 5,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 19,
            "time": 35.25
        }
    ],
    [
        {
            "defaultXPos": "147.59",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "E4",
            "dot": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "eighth",
            "measure": 19,
            "time": 35.5
        }
    ],
    [
        {
            "defaultXPos": "191.65",
            "isChord": false,
            "chordIdx": false,
            "tie": false,
            "pitch": "D4",
            "dot": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "color": "white",
            "show": false,
            "duration": "half",
            "measure": 19,
            "time": 35.75
        }
    ]
];