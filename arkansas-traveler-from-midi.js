// this is the actual output note object
// from the videoPlayer

// Please note that the first 6 notes
// (2-4, ?, ?, ?, 2-4, ?, ?, ?) are not 
// included in the musicxml tablature

// song notes matches after these notes

// song: Arkansas Traveler

const objectFromMidi = [
    [
        {
            "isLine": false,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 0,
            "offset": 0,
            "isSymbol": true
        }
    ],
    [
        {
            "isLine": false,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 0,
            "offset": 0,
            "isSymbol": true
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 0,
            "offset": 0
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 0,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": 0,
            "time": 0,
            "duration": 0.10394737708333333
        }
    ],
    [
        {
            "midi": 64,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 9600,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": "?",
                "string": 1
            },
            "offset": 0,
            "time": 0.631579,
            "duration": 0.10394737708333335
        }
    ],
    [
        {
            "midi": 64,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 19200,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": "?",
                "string": 1
            },
            "offset": 0,
            "time": 1.263158,
            "duration": 0.10394737708333324
        }
    ],
    [
        {
            "midi": 64,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 28800,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": "?",
                "string": 1
            },
            "offset": 0,
            "time": 1.8947370000000001,
            "duration": 0.10394737708333324
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 38400,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": 0,
            "time": 2.526316,
            "duration": 0.10394737708333368
        }
    ],
    [
        {
            "midi": 64,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 48000,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": "?",
                "string": 1
            },
            "offset": 0,
            "time": 3.157895,
            "duration": 0.10394737708333368
        }
    ],
    [
        {
            "midi": 64,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 57600,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": "?",
                "string": 1
            },
            "offset": 0,
            "time": 3.7894740000000002,
            "duration": 0.10394737708333324
        }
    ],
    [
        {
            "midi": 64,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 67200,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": "?",
                "string": 1
            },
            "offset": 0,
            "time": 4.421053,
            "duration": 0.10394737708333412
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 72000,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": -23.67,
            "time": 4.7368425,
            "duration": 0.10394737708333412
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 73600,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": -47.34,
            "time": 4.842105666666667,
            "duration": 0.10394737708333324
        }
    ],
    [
        {
            "midi": 73,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 75200,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": 6,
                "string": 4
            },
            "offset": -71.01,
            "time": 4.947368833333333,
            "duration": 0.10394737708333412
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 76800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -67.01,
            "time": 5.052632,
            "duration": 0.3144737104166664
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 81600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": -63.010000000000005,
            "time": 5.3684215,
            "duration": 0.3144737104166664
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 86400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": -59.010000000000005,
            "time": 5.684211,
            "duration": 0.3144737104166664
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 91200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -55.010000000000005,
            "time": 6.0000005,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 96000,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": -51.010000000000005,
            "time": 6.31579,
            "duration": 0.6302632104166666
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 105600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": -47.010000000000005,
            "time": 6.947369,
            "duration": 0.3144737104166664
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 105600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -47.010000000000005,
            "time": 6.947369,
            "duration": 0.3144737104166664
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 110400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": -43.010000000000005,
            "time": 7.2631585,
            "duration": 0.3144737104166664
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 110400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -43.010000000000005,
            "time": 7.2631585,
            "duration": 0.3144737104166664
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 7.2631585,
            "offset": -39.010000000000005
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 115200,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": -35.010000000000005,
            "time": 7.5789480000000005,
            "duration": 0.6302632104166666
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 124800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": -31.010000000000005,
            "time": 8.210527,
            "duration": 0.3144737104166655
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 124800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -31.010000000000005,
            "time": 8.210527,
            "duration": 0.3144737104166655
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 129600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": -27.010000000000005,
            "time": 8.5263165,
            "duration": 0.3144737104166673
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 129600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -27.010000000000005,
            "time": 8.5263165,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 134400,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -23.010000000000005,
            "time": 8.842106,
            "duration": 0.6302632104166666
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 144000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -19.010000000000005,
            "time": 9.473685,
            "duration": 0.3144737104166673
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 144000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": -19.010000000000005,
            "time": 9.473685,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 148800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -15.010000000000005,
            "time": 9.7894745,
            "duration": 0.3144737104166655
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 148800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": -15.010000000000005,
            "time": 9.7894745,
            "duration": 0.3144737104166655
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 9.7894745,
            "offset": -11.010000000000005
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 153600,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": -7.010000000000005,
            "time": 10.105264,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 163200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": -3.010000000000005,
            "time": 10.736843,
            "duration": 0.3144737104166655
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 163200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": -3.010000000000005,
            "time": 10.736843,
            "duration": 0.3144737104166655
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 168000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0.9899999999999949,
            "time": 11.0526325,
            "duration": 0.3144737104166655
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 168000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0.9899999999999949,
            "time": 11.0526325,
            "duration": 0.3144737104166655
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 172800,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 11.368422,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 182400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 12.000001,
            "duration": 0.3144737104166673
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 182400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 12.000001,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 187200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 12.3157905,
            "duration": 0.3144737104166655
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 187200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 12.3157905,
            "duration": 0.3144737104166655
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 192000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0,
            "time": 12.63158,
            "duration": 0.3144737104166655
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 12.63158,
            "offset": 0
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 196800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 12.9473695,
            "duration": 0.3144737104166655
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 201600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0,
            "time": 13.263159,
            "duration": 0.3144737104166655
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 206400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 13.5789485,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 211200,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": 0,
            "time": 13.894738,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 220800,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": 0,
            "time": 14.526317,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 230400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 15.157896000000001,
            "duration": 0.3144737104166655
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 15.157896000000001,
            "offset": 0
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 235200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 15.4736855,
            "duration": 0.3144737104166655
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 240000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0,
            "time": 15.789475,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 244800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 16.1052645,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 249600,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": 0,
            "time": 16.421054,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 259200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": 0,
            "time": 17.052633,
            "duration": 0.3144737104166673
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 259200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 17.052633,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 264000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": 0,
            "time": 17.3684225,
            "duration": 0.3144737104166637
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 264000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 17.3684225,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 17.3684225,
            "offset": 0
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 268800,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": 0,
            "time": 17.684212,
            "duration": 0.6302632104166683
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 278400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": 0,
            "time": 18.315791,
            "duration": 0.3144737104166637
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 278400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 18.315791,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 283200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": 0,
            "time": 18.6315805,
            "duration": 0.3144737104166673
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 283200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 18.6315805,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 288000,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 18.94737,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 297600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 19.578949,
            "duration": 0.3144737104166637
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 297600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 19.578949,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 302400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 19.8947385,
            "duration": 0.3144737104166673
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 302400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 19.8947385,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 307200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 20.210528,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 85,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 312000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 2
            },
            "offset": 0,
            "time": 20.5263175,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 20.5263175,
            "offset": 0
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 316800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 20.842107,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 321600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 21.1578965,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 83,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 326400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 2
            },
            "offset": 0,
            "time": 21.473686,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 85,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 331200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 2
            },
            "offset": 0,
            "time": 21.7894755,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 336000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 22.105265,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 83,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 340800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 2
            },
            "offset": 0,
            "time": 22.4210545,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 22.4210545,
            "offset": 0
        }
    ],
    [
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 345600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 22.736844,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 350400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 23.0526335,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 79,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 355200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 3
            },
            "offset": 0,
            "time": 23.368423,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 360000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0,
            "time": 23.6842125,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 364800,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 24.000002,
            "duration": 0.6302632104166683
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 379200,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": -23.67,
            "time": 24.9473705,
            "duration": 0.10394737708332968
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 380800,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": -47.34,
            "time": 25.052633666666665,
            "duration": 0.10394737708333324
        }
    ],
    [
        {
            "midi": 73,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 382400,
            "durationTicks": 1580,
            "color": "white",
            "show": false,
            "name": {
                "position": 6,
                "string": 4
            },
            "offset": -71.01,
            "time": 25.157896833333336,
            "duration": 0.10394737708333324
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 384000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -67.01,
            "time": 25.26316,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 25.26316,
            "offset": -63.010000000000005
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 388800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": -59.010000000000005,
            "time": 25.5789495,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 393600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": -55.010000000000005,
            "time": 25.894739,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 398400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -51.010000000000005,
            "time": 26.2105285,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 403200,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": -47.010000000000005,
            "time": 26.526318,
            "duration": 0.6302632104166683
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 412800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": -43.010000000000005,
            "time": 27.157897,
            "duration": 0.3144737104166708
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 412800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -43.010000000000005,
            "time": 27.157897,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 417600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": -39.010000000000005,
            "time": 27.4736865,
            "duration": 0.3144737104166673
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 417600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -39.010000000000005,
            "time": 27.4736865,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 27.4736865,
            "offset": -35.010000000000005
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 422400,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": -31.010000000000005,
            "time": 27.789476,
            "duration": 0.6302632104166683
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 432000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": -27.010000000000005,
            "time": 28.421055,
            "duration": 0.3144737104166673
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 432000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -27.010000000000005,
            "time": 28.421055,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 436800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": -23.010000000000005,
            "time": 28.7368445,
            "duration": 0.3144737104166673
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 436800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -23.010000000000005,
            "time": 28.7368445,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 441600,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -19.010000000000005,
            "time": 29.052634,
            "duration": 0.6302632104166683
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 451200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -15.010000000000005,
            "time": 29.684213,
            "duration": 0.3144737104166673
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 451200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": -15.010000000000005,
            "time": 29.684213,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 456000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": -11.010000000000005,
            "time": 30.0000025,
            "duration": 0.3144737104166673
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 456000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": -11.010000000000005,
            "time": 30.0000025,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 460800,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": -7.010000000000005,
            "time": 30.315792000000002,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 30.315792000000002,
            "offset": -3.010000000000005
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 470400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0.9899999999999949,
            "time": 30.947371,
            "duration": 0.3144737104166673
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 470400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0.9899999999999949,
            "time": 30.947371,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 475200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0,
            "time": 31.2631605,
            "duration": 0.3144737104166673
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 475200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 31.2631605,
            "duration": 0.3144737104166673
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 480000,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 31.57895,
            "duration": 0.6302632104166719
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 489600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 32.210529,
            "duration": 0.3144737104166637
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 489600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 32.210529,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 494400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 32.5263185,
            "duration": 0.3144737104166637
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 494400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 32.5263185,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 499200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0,
            "time": 32.842108,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 32.842108,
            "offset": 0
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 504000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 33.1578975,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 508800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0,
            "time": 33.473687,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 513600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 33.7894765,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 518400,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": 0,
            "time": 34.105266,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 528000,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": 0,
            "time": 34.736845,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 537600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0,
            "time": 35.368424,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 35.368424,
            "offset": 0
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 542400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 35.6842135,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 547200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0,
            "time": 36.000003,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 552000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 36.3157925,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 556800,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": 0,
            "time": 36.631582,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 566400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": 0,
            "time": 37.263161,
            "duration": 0.3144737104166708
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 566400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 37.263161,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 71,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 571200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 4
            },
            "offset": 0,
            "time": 37.5789505,
            "duration": 0.3144737104166708
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 571200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 37.5789505,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 576000,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": 0,
            "time": 37.89474,
            "duration": 0.6302632104166719
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 585600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": 0,
            "time": 38.526319,
            "duration": 0.3144737104166637
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 585600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 38.526319,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 38.526319,
            "offset": 0
        }
    ],
    [
        {
            "midi": 69,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 590400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 4
            },
            "offset": 0,
            "time": 38.8421085,
            "duration": 0.3144737104166637
        },
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 590400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 38.8421085,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 595200,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 39.157898,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 604800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 39.789477,
            "duration": 0.3144737104166708
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 604800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 39.789477,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 609600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 40.1052665,
            "duration": 0.3144737104166708
        },
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 609600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 40.1052665,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 614400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 40.421056,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 85,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 619200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 2
            },
            "offset": 0,
            "time": 40.7368455,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 624000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 41.052635,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 41.052635,
            "offset": 0
        }
    ],
    [
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 628800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 41.3684245,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 83,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 633600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 2
            },
            "offset": 0,
            "time": 41.684214,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 85,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 638400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 2
            },
            "offset": 0,
            "time": 42.0000035,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 643200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 42.315793,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 83,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 648000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 2
            },
            "offset": 0,
            "time": 42.6315825,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 652800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 42.947372,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 657600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 43.2631615,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 79,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 662400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 3
            },
            "offset": 0,
            "time": 43.578951,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 43.578951,
            "offset": 0
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 667200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0,
            "time": 43.8947405,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 672000,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 44.21053,
            "duration": 0.6302632104166719
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 681600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 44.842109,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 686400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 45.1578985,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 93,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 691200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 1
            },
            "offset": 0,
            "time": 45.473688,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 45.473688,
            "offset": 0
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 696000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 45.7894775,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 700800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 46.105267,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 93,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 705600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 1
            },
            "offset": 0,
            "time": 46.4210565,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 710400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 46.736846,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 715200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 47.0526355,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 720000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 47.368425,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 724800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 47.6842145,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 47.6842145,
            "offset": 0
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 729600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 48.000004,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 734400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 48.3157935,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 739200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 48.631583,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 744000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 48.9473725,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 748800,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 49.263162,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 758400,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 49.894741,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 768000,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 50.52632,
            "duration": 0.6302632104166719
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 50.52632,
            "offset": 0
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 777600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 51.157899,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 782400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 51.4736885,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 787200,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 51.789478,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 796800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 52.421057,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 801600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 52.7368465,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 806400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 53.052636,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 811200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 53.3684255,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 816000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 53.684215,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 53.684215,
            "offset": 0
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 820800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 54.0000045,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 825600,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 54.315794,
            "duration": 0.6302632104166719
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 835200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 54.947373,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 840000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 55.2631625,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 93,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 844800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 1
            },
            "offset": 0,
            "time": 55.578952,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 849600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 55.8947415,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 854400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 56.210531,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 93,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 859200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 1
            },
            "offset": 0,
            "time": 56.5263205,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 56.5263205,
            "offset": 0
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 864000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 56.84211,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 868800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 57.1578995,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 873600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 57.473689,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 878400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 57.7894785,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 883200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 58.105268,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 888000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 58.4210575,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 892800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 58.736847,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 58.736847,
            "offset": 0
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 897600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 59.0526365,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 902400,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 59.368426,
            "duration": 0.6302632104166719
        }
    ],
    [
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 912000,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 60.000005,
            "duration": 0.6302632104166648
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 921600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 60.631584000000004,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 85,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 926400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 2
            },
            "offset": 0,
            "time": 60.9473735,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 931200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 61.263163,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 936000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 61.5789525,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 83,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 940800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 2
            },
            "offset": 0,
            "time": 61.894742,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 61.894742,
            "offset": 0
        }
    ],
    [
        {
            "midi": 85,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 945600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 2
            },
            "offset": 0,
            "time": 62.2105315,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 950400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 62.526321,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 83,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 955200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 2
            },
            "offset": 0,
            "time": 62.8421105,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 960000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 63.1579,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 964800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 63.4736895,
            "duration": 0.3144737104166708
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 63.4736895,
            "offset": 0
        }
    ],
    [
        {
            "midi": 79,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 969600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 3
            },
            "offset": 0,
            "time": 63.789479,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 974400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0,
            "time": 64.1052685,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 979200,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 64.421058,
            "duration": 0.6302632104166719
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 988800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 65.052637,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 993600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 65.3684265,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 93,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 998400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 1
            },
            "offset": 0,
            "time": 65.684216,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1003200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 66.0000055,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 66.0000055,
            "offset": 0
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1008000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 66.315795,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 93,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1012800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 1
            },
            "offset": 0,
            "time": 66.6315845,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1017600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 66.947374,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1022400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 67.2631635,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1027200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 67.578953,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1032000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 67.8947425,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1036800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 68.210532,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1041600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 68.5263215,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 68.5263215,
            "offset": 0
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1046400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 68.842111,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1051200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 69.1579005,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1056000,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 69.47369,
            "duration": 0.6302632104166577
        }
    ],
    [
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1065600,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 70.105269,
            "duration": 0.6302632104166577
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1075200,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 70.736848,
            "duration": 0.6302632104166719
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1084800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 71.368427,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1089600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 71.6842165,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 71.6842165,
            "offset": 0
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1094400,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 72.000006,
            "duration": 0.6302632104166719
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1104000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 72.631585,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1108800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 72.9473745,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1113600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 73.263164,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1118400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 73.5789535,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1123200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 73.894743,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1128000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 74.2105325,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1132800,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 74.526322,
            "duration": 0.6302632104166719
        }
    ],
    [
        {
            "isLine": true,
            "color": "white",
            "show": false,
            "duration": 0,
            "time": 74.526322,
            "offset": 0
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1142400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 75.157901,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1147200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 75.4736905,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 93,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1152000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 1
            },
            "offset": 0,
            "time": 75.78948,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1156800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 76.1052695,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1161600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 76.421059,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 93,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1166400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 1
            },
            "offset": 0,
            "time": 76.7368485,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1171200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 77.052638,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1176000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 77.3684275,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1180800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 77.684217,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 91,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1185600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 3,
                "string": 1
            },
            "offset": 0,
            "time": 78.0000065,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1190400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 78.315796,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1195200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 78.6315855,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1200000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 78.947375,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 90,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1204800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 1
            },
            "offset": 0,
            "time": 79.2631645,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 88,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1209600,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 1
            },
            "offset": 0,
            "time": 79.578954,
            "duration": 0.6302632104166719
        }
    ],
    [
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1219200,
            "durationTicks": 9580,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 80.210533,
            "duration": 0.6302632104166719
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1228800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 80.842112,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 85,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1233600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 2
            },
            "offset": 0,
            "time": 81.1579015,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1238400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 81.473691,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1243200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 81.7894805,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 83,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1248000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 2
            },
            "offset": 0,
            "time": 82.10527,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 85,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1252800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 2
            },
            "offset": 0,
            "time": 82.4210595,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 86,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1257600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 2
            },
            "offset": 0,
            "time": 82.736849,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 83,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1262400,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 2
            },
            "offset": 0,
            "time": 83.0526385,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 81,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1267200,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 2
            },
            "offset": 0,
            "time": 83.368428,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 78,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1272000,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 4,
                "string": 3
            },
            "offset": 0,
            "time": 83.6842175,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 79,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1276800,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 5,
                "string": 3
            },
            "offset": 0,
            "time": 84.000007,
            "duration": 0.31447371041667793
        }
    ],
    [
        {
            "midi": 76,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1281600,
            "durationTicks": 4780,
            "color": "white",
            "show": false,
            "name": {
                "position": 2,
                "string": 3
            },
            "offset": 0,
            "time": 84.3157965,
            "duration": 0.3144737104166637
        }
    ],
    [
        {
            "midi": 74,
            "velocity": 0.6299212598425197,
            "noteOffVelocity": 0,
            "ticks": 1286400,
            "durationTicks": 19180,
            "color": "white",
            "show": false,
            "name": {
                "position": 0,
                "string": 3
            },
            "offset": 0,
            "time": 84.631586,
            "duration": 1.261842210416674
        }
    ]
]
